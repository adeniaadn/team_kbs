@extends('dashboard.main')

@section('subjudul')
<span class="judul-dashboard">Aplikasi dan OPD</span>
<span class="judul-dashboard px-3">|</span>
<span class="subjudul-dashboard">Tabel Aplikasi dan OPD</span>

@endsection

@section('content')
<div id="wrapper" style="background-color: black;">
  <ul class="navbar-nav sidebar sidebar-dark accordion fixed-top" style="background-color: white;" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="row sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="p-0 sidebar-brand-icon">
          <i class="bi bi-person-circle"></i>
        </div>
        <div class="p-0 sidebar-brand-text">{{ Auth::user()->name  }}</div>
        <div class="p-0 sidebar-brand-text">Admin</div>
    </a>


    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
      <a class="nav-link" href='{{ route('dashboard') }}'>
          <i class="bi bi-speedometer2 bi-color"></i>
          <span>Dashboard</span>
      </a>
    </li>
    <div class="accordion accordion-flush" id="accordionFlushExample">
      {{-- data website --}}
      <div class="accordion-item">
        {{-- diisi menu yg ada sub menu nya --}}
        <button class="ps-0 accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
          <li class="nav-item">
            <a class="nav-link">
                <i class="bi bi-clipboard-data bi-color"></i>
                <span>Database Website</span>
            </a>
          </li>
        </button>
        <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
          <div class="accordion-body">
            {{-- diisi sub menu --}}
            <li class="nav-item">
              <a class="nav-link submenu" href='{{ route('datawebsite') }}'>
                  <i class="bi bi-clipboard-data bi-color"></i>
                  <span>Tabel Database Website</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link submenu" href='{{ url('/tambah-data') }}'>
                  <i class="bi bi-plus-circle-fill bi-color"></i>
                  <span>Tambah Database Website</span>
              </a>
            </li>
          </div>
        </div>
      </div>
      {{-- aplikasi dan opd --}}
      <div class="accordion-item">
        {{-- diisi menu yg ada sub menu nya --}}
        <button class="ps-0 accordion-button active" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
          <li class="nav-item active">
            <a class="nav-link">
                <i class="bi bi-table bi-color"></i>
                <span>Aplikasi dan OPD</span>
            </a>
          </li>
        </button>
        <div id="flush-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
          <div class="accordion-body">
            {{-- diisi sub menu --}}
            <li class="nav-item active">
              <a class="nav-link submenu" href='{{ url('/tabledataapp') }}'>
                  <i class="bi bi-table bi-color"></i>
                  <span>Tabel Master Aplikasi</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link submenu" href='{{ url('/tambah-data-app') }}'>
                  <i class="bi bi-plus-circle-fill bi-color"></i>
                  <span>Tambah Master Aplikasi</span>
              </a>
            </li>
          </div>
        </div>
      </div>
      {{-- sumber data --}}
      <div class="accordion-item">
        {{-- diisi menu yg ada sub menu nya --}}
        <button class="ps-0 accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
          <li class="nav-item">
            <a class="nav-link">
                <i class="bi bi-table bi-color"></i>
                <span>Sumber Data</span>
            </a>
          </li>
        </button>
        <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
          <div class="accordion-body">
            {{-- diisi sub menu --}}
            <li class="nav-item">
              <a class="nav-link submenu" href='{{ url('/datasumber') }}'>
                  <i class="bi bi-table bi-color"></i>
                  <span>Tabel Sumber Data</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link submenu" href='{{ url('/tambah-data-sumber') }}'>
                  <i class="bi bi-plus-circle-fill bi-color"></i>
                  <span>Tambah Sumber Data</span>
              </a>
            </li>
          </div>
        </div>
      </div>
    </div>
    <li class="nav-item">
      <a class="nav-link" href='{{ url('/setting') }}'>
          <i class="bi bi-gear-fill bi-color"></i>
          <span>Pengaturan</span>
      </a>
    </li>
    <li class="nav-item">
      <form method="POST" action="{{ route('logout') }}">
        @csrf
        <a class="nav-link" href="route('logout')" onclick="event.preventDefault(); this.closest('form').submit();">
          <i class="bi bi-box-arrow-left bi-color"></i>
          <span>Keluar</span>
        </a>
      </form>
    </li>
  </ul>
</div>
@endsection

@section('content2')
<div>
  <form class="searchtbl" action="/tabledataapp/search" method="get">
    <button type="submit" class="search-button"><i class="bi bi-search icon-search2"></i></button>
    <input class="search-container" type="search" name="search" placeholder="Cari dalam tabel" aria-label="Search">
    <button type="button" class="tambahdata" onclick="location.href='{{ url('/tambah-data-app') }}'">
      <p>+ Master Aplikasi</p>
    </button>
  </form>
</div>
<div class="bdr table"><br>
  <table class="table-hover table-responsive">
    <thead>
      <tr class="thead">
        <th>No.</th>
        <th>Nama Aplikasi</th>
        <th>Pemilik Aplikasi</th>
        <th>URL Aplikasi</th>
        <th class="th">Tindakan</th>
      </tr>
    </thead>
    @php
    $no=1;
    @endphp
    @foreach ( $data as $item)
    <tr>
    <td>{{ $no++ }}</td>
    <td>{{ $item->nama_app }}</td>
    <td>{{ $item->nama }}</td>
    <td>{{ $item->url }}</td>
    <td>
        <a href="{{ url('data-app/' . $item->id) }}" class="btn btn-warning"> Edit </a>
        <form action="{{ url('destroy-data-app/'.$item->id) }}" method="POST" class="d-inline">
          @csrf
          @method('DELETE')
          <button class="btn btn-danger"> Delete</button>
        </form>
    </td>
    </tr>
    @endforeach 
  </table>
</div>
{{ $data->links() }}
@endsection
  {{-- <div class="d-flex justify-content-end halaman">
    <nav aria-label="Page navigation example">
      <ul class="pagination pagination-sm">
        <li class="page-item disabled">
          <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Sebelumnya</a>
        </li>
        <li class="page-item active"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item">
          <a class="page-link" href="#">Selanjutnya</a>
        </li>
      </ul>
    </nav>
  </div> --}}



