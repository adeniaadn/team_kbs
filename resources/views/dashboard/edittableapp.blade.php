@extends('dashboard.main')

@section('subjudul')
<span class="judul-dashboard">Aplikasi dan OPD</span>
<span class="judul-dashboard px-3">|</span>
<span class="subjudul-dashboard">Edit Tabel Aplikasi dan OPD</span>

@endsection

@section('content')
<div id="wrapper" style="background-color: black;">
  <ul class="navbar-nav sidebar sidebar-dark accordion fixed-top" style="background-color: white;" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="row sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="p-0 sidebar-brand-icon">
          <i class="bi bi-person-circle"></i>
        </div>
        <div class="p-0 sidebar-brand-text">{{ Auth::user()->name  }}</div>
        <div class="p-0 sidebar-brand-text">Admin</div>
    </a>


    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
      <a class="nav-link" href='{{ route('dashboard') }}'>
          <i class="bi bi-speedometer2 bi-color"></i>
          <span>Dashboard</span>
      </a>
    </li>
    <div class="accordion accordion-flush" id="accordionFlushExample">
      {{-- data website --}}
      <div class="accordion-item">
        {{-- diisi menu yg ada sub menu nya --}}
        <button class="ps-0 accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
          <li class="nav-item">
            <a class="nav-link">
                <i class="bi bi-clipboard-data bi-color"></i>
                <span>Database Website</span>
            </a>
          </li>
        </button>
        <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
          <div class="accordion-body">
            {{-- diisi sub menu --}}
            <li class="nav-item">
              <a class="nav-link submenu" href='{{ route('datawebsite') }}'>
                  <i class="bi bi-clipboard-data bi-color"></i>
                  <span>Tabel Database Website</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link submenu" href='{{ url('/tambah-data') }}'>
                  <i class="bi bi-plus-circle-fill bi-color"></i>
                  <span>Tambah Database Website</span>
              </a>
            </li>
          </div>
        </div>
      </div>
      {{-- aplikasi dan opd --}}
      <div class="accordion-item">
        {{-- diisi menu yg ada sub menu nya --}}
        <button class="ps-0 accordion-button active" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
          <li class="nav-item active">
            <a class="nav-link">
                <i class="bi bi-table bi-color"></i>
                <span>Aplikasi dan OPD</span>
            </a>
          </li>
        </button>
        <div id="flush-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
          <div class="accordion-body">
            {{-- diisi sub menu --}}
            <li class="nav-item active">
              <a class="nav-link submenu" href='{{ url('/tabledataapp') }}'>
                  <i class="bi bi-table bi-color"></i>
                  <span>Tabel Master Aplikasi</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link submenu" href='{{ url('/tambah-data-app') }}'>
                  <i class="bi bi-plus-circle-fill bi-color"></i>
                  <span>Tambah Master Aplikasi</span>
              </a>
            </li>
          </div>
        </div>
      </div>
      {{-- sumber data --}}
      <div class="accordion-item">
        {{-- diisi menu yg ada sub menu nya --}}
        <button class="ps-0 accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
          <li class="nav-item">
            <a class="nav-link">
                <i class="bi bi-table bi-color"></i>
                <span>Sumber Data</span>
            </a>
          </li>
        </button>
        <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
          <div class="accordion-body">
            {{-- diisi sub menu --}}
            <li class="nav-item">
              <a class="nav-link submenu" href='{{ url('/datasumber') }}'>
                  <i class="bi bi-table bi-color"></i>
                  <span>Tabel Sumber Data</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link submenu" href='{{ url('/tambah-data-sumber') }}'>
                  <i class="bi bi-plus-circle-fill bi-color"></i>
                  <span>Tambah Sumber Data</span>
              </a>
            </li>
          </div>
        </div>
      </div>
    </div>
    <li class="nav-item">
      <a class="nav-link" href='{{ url('/setting') }}'>
          <i class="bi bi-gear-fill bi-color"></i>
          <span>Pengaturan</span>
      </a>
    </li>
    <li class="nav-item">
      <form method="POST" action="{{ route('logout') }}">
        @csrf
        <a class="nav-link" href="route('logout')" onclick="event.preventDefault(); this.closest('form').submit();">
          <i class="bi bi-box-arrow-left bi-color"></i>
          <span>Keluar</span>
        </a>
      </form>
    </li>
  </ul>
</div>

@endsection

@section('content2') 
              <form action="{{ url('edit-data-app') }}/{{ $data->id }}" method="POST" class="form">
                @csrf
                <div class="row mb-3">
                    <label for="nama" class="col-sm-2 col-form-label text-dark judul">Nama Aplikasi </label>
                    <div class="col-sm-10">
                      <input type="text" value="{{ $data->nama_app }}" name="nama" class="form-control" placeholder="Masukkan Nama Aplikasi">
                    </div>
                  </div>

                  <div class="row mb-3">
                    <label for="pemilik" class="col-sm-2 col-form-label text-dark judul">Pemilik Aplikasi </label>
                    <div class="col-sm-10">
                      <select name="pemilik" id="" class="form-select">
                          @foreach ($datamaster as $item)
                            <option value="{{ $item->id }}" @if ($item->id==$data->id_pemilik)
                            selected
                            @endif> {{ $item->nama }} </option>
                          @endforeach
                      </select>
                    </div>
                  </div>

                  {{-- <div class="row mb-3">
                    <label for="id_sumber" class="col-sm-2 col-form-label text-dark judul">Sumber Data </label>
                    <div class="col-sm-10">
                      <select name="sumber_data" class="form-select">
                        @foreach ($datasumber as $item)
                          <option value="{{ $item->id }}" @if ($item->id==$data->id_app) 
                          selected 
                          @endif>{{ $item->endpoint }} | {{ $item->api }}</option>
                        @endforeach
                    </select>
                    </div>
                  </div> --}}

                  <div class="row mb-3">
                    <label for="url" class="col-sm-2 col-form-label text-dark judul">URL Aplikasi </label>
                    <div class="col-sm-10">
                      <input type="text" value="{{ $data->url }}" name="url" class="form-control" placeholder="Masukkan URL Aplikasi">
                      </select>
                    </div>
                  </div>
                <div class="button">
                  <button style="background-color:grey" type="reset" class="btn btn-primary">Batal</button>
                  <button type="submit" class="btn btn-primary" id="simpan" >Simpan</button>
                </div>
              </form>
            </div>
          </div>
@endsection
