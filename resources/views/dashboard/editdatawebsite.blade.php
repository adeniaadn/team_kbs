@extends('dashboard.main')

@section('subjudul')
<span class="judul-dashboard">Database Website</span>
<span class="judul-dashboard px-3">|</span>
<span class="subjudul-dashboard">Edit Database Website</span>

@endsection

@section('content')
<div id="wrapper" style="background-color: black;">
  <ul class="navbar-nav sidebar sidebar-dark accordion fixed-top" style="background-color: white;" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="row sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="p-0 sidebar-brand-icon">
          <i class="bi bi-person-circle"></i>
        </div>
        <div class="p-0 sidebar-brand-text">{{ Auth::user()->name  }}</div>
        <div class="p-0 sidebar-brand-text">Admin</div>
    </a>


    <!-- Nav Item - Dashboard -->
    <li class="nav-item ">
      <a class="nav-link" href='{{ route('dashboard') }}'>
          <i class="bi bi-speedometer2 bi-color"></i>
          <span>Dashboard</span>
      </a>
    </li>
    <div class="accordion accordion-flush" id="accordionFlushExample">
      {{-- data website --}}
      <div class="accordion-item">
        {{-- diisi menu yg ada sub menu nya --}}
        <button class="ps-0 accordion-button active" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
          <li class="nav-item active">
            <a class="nav-link">
                <i class="bi bi-clipboard-data bi-color"></i>
                <span>Database Website</span>
            </a>
          </li>
        </button>
        <div id="flush-collapseOne" class="accordion-collapse collapse show" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
          <div class="accordion-body">
            {{-- diisi sub menu --}}
            <li class="nav-item active">
              <a class="nav-link submenu" href='{{ route('datawebsite') }}'>
                  <i class="bi bi-clipboard-data bi-color"></i>
                  <span>Tabel Database Website</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link submenu" href='{{ url('/tambah-data') }}'>
                  <i class="bi bi-plus-circle-fill bi-color"></i>
                  <span>Tambah Database Website</span>
              </a>
            </li>
          </div>
        </div>
      </div>
      {{-- aplikasi dan opd --}}
      <div class="accordion-item">
        {{-- diisi menu yg ada sub menu nya --}}
        <button class="ps-0 accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
          <li class="nav-item">
            <a class="nav-link">
                <i class="bi bi-table bi-color"></i>
                <span>Aplikasi dan OPD</span>
            </a>
          </li>
        </button>
        <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
          <div class="accordion-body">
            {{-- diisi sub menu --}}
            <li class="nav-item">
              <a class="nav-link submenu" href='{{ url('/tabledataapp') }}'>
                  <i class="bi bi-table bi-color"></i>
                  <span>Tabel Master Aplikasi</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link submenu" href='{{ url('/tambah-data-app') }}'>
                  <i class="bi bi-plus-circle-fill bi-color"></i>
                  <span>Tambah Master Aplikasi</span>
              </a>
            </li>
          </div>
        </div>
      </div>
      {{-- sumber data --}}
      <div class="accordion-item">
        {{-- diisi menu yg ada sub menu nya --}}
        <button class="ps-0 accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
          <li class="nav-item">
            <a class="nav-link">
                <i class="bi bi-table bi-color"></i>
                <span>Sumber Data</span>
            </a>
          </li>
        </button>
        <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
          <div class="accordion-body">
            {{-- diisi sub menu --}}
            <li class="nav-item">
              <a class="nav-link submenu" href='{{ url('/datasumber') }}'>
                  <i class="bi bi-table bi-color"></i>
                  <span>Tabel Sumber Data</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link submenu" href='{{ url('/tambah-data-sumber') }}'>
                  <i class="bi bi-plus-circle-fill bi-color"></i>
                  <span>Tambah Sumber Data</span>
              </a>
            </li>
          </div>
        </div>
      </div>
    </div>
    <li class="nav-item">
      <a class="nav-link" href='{{ url('/setting') }}'>
          <i class="bi bi-gear-fill bi-color"></i>
          <span>Pengaturan</span>
      </a>
    </li>
    <li class="nav-item">
      <form method="POST" action="{{ route('logout') }}">
        @csrf
        <a class="nav-link" href="route('logout')" onclick="event.preventDefault(); this.closest('form').submit();">
          <i class="bi bi-box-arrow-left bi-color"></i>
          <span>Keluar</span>
        </a>
      </form>
    </li>
  </ul>
</div>

@endsection

@section('content2')
          
              <form action="{{ url('data') }}/{{ $data->id }}" method="POST" class="form">
                @csrf
                <div class="row mb-3">
                  <label for="nama_laporan" class="col-sm-2 col-form-label text-dark judul">Nama Laporan </label>
                  <div class="col-sm-10">
                    <input type="text" name="nama_laporan" value="{{ $data->judul }}" class="form-control" placeholder="Nama Laporan">
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="id_grup" class="col-sm-2 col-form-label text-dark judul">Grup Laporan </label>
                  <div class="col-sm-10">
                    @php
                      $grup=array('FMS', 'URL')
                    @endphp
                    <select name="id_grup" class="form-select">
                        @for($i = 0; $i < 2; $i++)
                          <option value="{{ $grup[$i] }}" @if ($grup[$i]=='FMS'||$grup[$i]=='URL')
                            selected
                            @endif>{{ $grup[$i] }}</option>
                        @endfor
                    </select>
                  </div>
                </div>

                <div class="row mb-3">
                    <label for="id_master" class="col-sm-2 col-form-label text-dark judul">Master Aplikasi </label>
                    <div class="col-sm-10">
                      <select name="id_master" class="form-select">
                          @foreach ($datamaster as $item)
                            <option value="{{ $item->id }}" @if ($item->id==$data->id_app)
                            selected
                            @endif>{{ $item->nama_app }} - {{ $item->nama }}</option>
                          @endforeach
                      </select>
                    </div>
                  </div>

                <div class="row mb-3">
                  <label for="kode_laporan" class="col-sm-2 col-form-label text-dark judul">Kode Laporan </label>
                  <div class="col-sm-10">
                    <input type="text" name="kode_laporan" value="{{ $data->kode }}" class="form-control" placeholder="Kode Laporan">
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="id_sumber" class="col-sm-2 col-form-label text-dark judul">Sumber Data </label>
                  <div class="col-sm-10">
                    <select name="sumber_data" class="form-select">
                      @foreach ($datasumber as $item)
                        <option value="{{ $item->id }}" @if ($item->id==$data->id_app) 
                        selected 
                        @endif>{{ $item->endpoint }} | {{ $item->api }}</option>
                      @endforeach
                  </select>
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="deskripsi" class="col-sm-2 col-form-label text-dark judul">Deskripsi </label>
                  <div class="col-sm-10">
                    <input type="text" name="deskripsi" value="{{ $data->deskripsi }}" class="form-control" placeholder="Masukkan Deskripsi Laporan">
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="gambar" class="col-sm-2 col-form-label text-dark judul">Gambar </label>
                  <div class="col-sm-10">
                    <img width="200px" src="{{asset('assets/images/'.$data->gambar)}}">
                    <input type="file" name="gambar" value="{{ $data->gambar }}" class="form-control" placeholder="Masukkan File Gambar">
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="video" class="col-sm-2 col-form-label text-dark judul">Video </label>
                  <div class="col-sm-10">
                    <input type="text" name="video" value="{{ $data->video }}" class="form-control" placeholder="Masukkan URL Video">
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="dokumen" class="col-sm-2 col-form-label text-dark judul">Dokumen </label>
                  <div class="col-sm-10">
                    <a href="{{ asset('assets/dokumen/'.$data->dokumen) }}" download>Download</a>
                    <input type="file" name="dokumen" value="{{ $data->dokumen }}" class="form-control" placeholder="Masukkan File Dokumen">
                  </div>
                </div>

                <div class="button">
                  <button style="background-color:grey" type="reset" class="btn btn-primary">Batal</button>
                  <button type="submit" class="btn btn-primary" id="simpan" >Simpan</button>
                </div>
                
              </form>
            </div>
          </div>
@endsection