<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
        <!-- Custome Style -->
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/custome-style.css') }}">
    
        <!-- Logo title bar -->
        <link rel="icon" href="{{ asset('/assets/img/icon.png') }}" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&display=swap" rel="stylesheet">
    
        <title>Knowledge Based System</title>
    </head>
    <style>
        .regis p{
            font-style: normal;
            font-weight: 600;
            font-size: 50px;
            line-height: 50px;

            color: #00415A;

            text-shadow: 2px 2px 4px rgba(0, 65, 90, 0.56);
        }
        .input-regis{
            width: 364px;
            height: 46px;
            left: 898px;
            top: 396px;
            color: white;
            background: rgba(0, 65, 90, 0.56);
            border-radius: 10px;
        }
        .input-regis::-webkit-input-placeholder{
	        color: white;
        }
        .btn-regis{
            width: 150px;
            height: 40px;
            font-style: normal;
            font-weight: 600;
            font-size: 18px;
            text-transform: uppercase;
            color: #E5E5E5;
            background: #00415A;
            border-radius: 10px;
            filter: drop-shadow(4px 4px 4px rgba(0, 0, 0, 0.25));
        }
        .btn-regis:hover{
            color: #E5E5E5;
            background-color: #55c449;
        }
        .judul p{
            color: white;
            font-style: normal;
        }
        .judul1{
            font-weight: 650;
            font-size: 22px;
            line-height: 34px;
            text-shadow: 2px 2px 4px #C4C4C4;
        }
        .judul2{
            font-weight: 700;
            font-size: 50px;
            line-height: 64px;
            text-shadow: 2px 2px 4px #C4C4C4;
        }
        .judul3{
            font-weight: 200;
            font-size: 22px;
            line-height: 30px;
            text-shadow: 2px 2px 4px #C4C4C4;
        }
    </style>
<body>
    <div class="d-lg-flex justify-content-center">
        <div class="bd-highlight w-100">
            <div class="d-flex flex-column justify-content-center judul px-5" style="height: 100vh; background-color: #A8D1E0;">
                <p class="judul1 fst-italic mb-1">
                    Selamat Datang di
                </p>
                <p class="judul2 fst-italic mb-1">
                    Sistemin Bali
                </p>
                <p class="judul3 text-wrap">
                    Website Berbasis Knowledge Based System Khusus Provinsi Bali
                </p>
            </div>
        </div>
        <div class="bd-highlight w-100">
            <div class="d-flex flex-column justify-content-center align-items-center" style="height: 100vh; background-color: white;">
                <div class="p-2 mb-0 bd-highlight regis">
                    <p>Register</p>
                </div>
                <x-auth-validation-errors class="mb-0" style="font-size: 12px;" :errors="$errors" />
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="p-2 bd-highlight">
                        <label class="form-label">Nama</label>
                        <input type="nama" name="name" class="form-control input-regis" id="exampleInputEmail1" required>
                    </div>
                    <div class="p-2 bd-highlight">
                        <label class="form-label">Email</label>
                        <input type="email" name="email" class="form-control input-regis" id="exampleInputEmail1" required>
                    </div>
                    <div class="p-2 bd-highlight">
                        <label class="form-label">Kata Sandi</label>
                        <input type="password" name="password" class="form-control input-regis" id="exampleInputEmail1" required >
                    </div>
                    <div class="p-2 mb-2 bd-highlight">
                        <label class="form-label">Konfirmasi Kata Sandi</label>
                        <input type="password" name="password_confirmation" class="form-control input-regis" id="exampleInputEmail1" required >
                    </div>
                    <div class="p-2 bd-highlight text-center">
                        <button type="submit" class="btn btn-regis me-2">Register</button>
                        <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                            {{ __('Already registered?') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="{{ asset('/js/app.js') }}">
    </script>
</body>
</html>
