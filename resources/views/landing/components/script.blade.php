    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="{{ asset('/js/app.js') }}">
    </script>
    <script type="text/javascript" src="{{ asset('js/searchclient.js') }}"></script>
    <script>
        const getKeyword = () =>{
            let keyword = document.querySelector("#searchInput").value;
            window.location.href = '/user/search-results/' + keyword;
        }
    </script>