<!-- Footer section -->
<footer id="footer-user" class="d-flex align-items-center">
    <div class="container-fluid">
        <div class="container">
            <div class="row tagline-footer">
                <div class="col-6 col-lg-3 d-flex align-items-center">
                    <img src="{{ asset('assets/img/logo-bali.png') }}" class="logo-footer" alt="logo bali">
                </div>
                <div class="col-6 col-lg-3">
                    <h4>
                        Hubungi Kami
                    </h4>
                    <p>
                        Pemerintah Provinsi Bali
                    </p>
                    <p>
                        Jalan Letda Tantular,
                    </p>
                    <p>
                        Renon, Denpasar, Bali
                    </p>
                </div>
                <div class="col-6 col-lg-3 d-flex flex-column justify-content-center contact">
                    <p>
                        <span class="fw-bold">Phone:</span> +6281-234-567-890
                    </p>
                    <p>
                        <span class="fw-bold">Email:</span> help@pemprovbali.com
                    </p>
                </div>
                <div class="col-6 col-lg-3 d-flex justify-content-center align-items-center">
                    <a href="">
                        <i class="bi bi-instagram sosmed"></i>
                    </a>
                    <a href="">
                        <i class="bi bi-facebook sosmed"></i>
                    </a>
                    <a href="">
                        <i class="bi bi-youtube sosmed"></i>
                    </a>
                    <a href="">
                        <i class="bi bi-twitter sosmed"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>