<style>
    .form-control.personal-data{
        height: 36px;

        border-radius: 10px;
    }
</style>

<!-- Modal personal data -->
<div class="modal fade" id="detailGambar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <form action="post">
                <div class="modal-body">
                    <div class="container col-12">
                        <div class="row">
                            <img src="{{ asset('assets/img/gwk-bali.png') }}">
                            <div class="col-12 mt-3">
                                <h3 class="card-title mb-3" style="font-weight: 600; line-height: 20px; color:black;">
                                    Garuda Wisnu Kencana
                                </h3>
                                <p class="card-text" style="font-weight: 100; font-size: 16px; line-height: 18px; color: black; text-align: justify; ">
                                    Destinasi Wisata Paling Favorit di Bali dengan Patung GWK sebagai Tertinggi Ke Empat di Dunia Menampilkan 15 Pertunjukan Kesenian Bali, Tiap Hari, Tiap Jam.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-send mb-4"><i class="bi bi-download me-1" style="color: white; font-size: 1rem;"></i>Unduh</button>
                </div>
            </form>
        </div>
    </div>
</div>