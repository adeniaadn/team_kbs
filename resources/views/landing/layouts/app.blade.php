<!DOCTYPE html>
<html lang="en">
@include('landing.components.head')
<body>
    @include('landing.components.navbar')
    @yield('content')
    @include('landing.components.footer')
</body>
@include('landing.components.script')
</html>