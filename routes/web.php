<?php

use App\Http\Controllers\AppController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DatabaseWebsiteController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\SumberController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// user
Route::get('/', function () {
    return view('landing.contents.search');
});
Route::get('/user/search-results', function () {
    return view('landing.contents.allResult');
});
Route::get('/user/search-results-gambar', function () {
    return view('landing.contents.allResultGambar');
});
Route::get('/user/search-results-video', function () {
    return view('landing.contents.allResultVideo');
});
Route::get('/user/search-results-document', function () {
    return view('landing.contents.allResultDocument');
});
Route::get('/user/chat-admin', function () {
    return view('landing.contents.chat');
});
Route::get('/user/not-found', function () {
    return view('landing.contents.notFound');
});

//admin
Route::get('/dashboard', [DashboardController::class, 'index'])->middleware(['auth'])->name('dashboard');
Route::get('/datawebsite', [DatabaseWebsiteController::class, 'index'])->middleware(['auth'])->name('datawebsite');
Route::get('/tambah-data', [DatabaseWebsiteController::class, 'create'])->middleware(['auth'])->name('create.datawebsite');
Route::post('/tambah-data', [DatabaseWebsiteController::class, 'store'])->middleware(['auth'])->name('post.datawebsite');
Route::get('/data/{id}', [DatabaseWebsiteController::class, 'show'])->middleware(['auth'])->name('edit.datawebsite');
Route::post('/data/{id}', [DatabaseWebsiteController::class, 'update'])->middleware(['auth'])->name('update.datawebsite');
Route::delete('/destroy/{id}', [DatabaseWebsiteController::class, 'destroy'])->middleware(['auth'])->name('destroy.datawebsite');
Route::get('/datawebsite/search',[DatabaseWebsiteController::class, 'search']);

Route::get('/datasumber', [SumberController::class, 'index'])->middleware(['auth'])->name('datasumber');
Route::get('/tambah-data-sumber', [SumberController::class, 'create'])->middleware(['auth'])->name('create.datasumber');
Route::post('/tambah-data-sumber', [SumberController::class, 'store'])->middleware(['auth'])->name('post.datasumber');
Route::get('/data-sumber/{id}', [SumberController::class, 'show'])->middleware(['auth'])->name('edit.datasumber');
Route::post('/edit-data-sumber/{id}', [SumberController::class, 'update'])->middleware(['auth'])->name('update.datasumber');
Route::delete('/destroy-data-sumber/{id}', [SumberController::class, 'destroy'])->middleware(['auth'])->name('destroy.datasumber');
Route::get('/datasumber/search',[SumberController::class, 'search']);

Route::get('/tabledataapp', [AppController::class, 'index'])->middleware(['auth'])->name('tabledataapp');
Route::get('/tambah-data-app', [AppController::class, 'create'])->middleware(['auth'])->name('create.tabledataapp');
Route::post('/tambah-data-app', [AppController::class, 'store'])->middleware(['auth'])->name('post.tabledataapp');
Route::get('/data-app/{id}', [AppController::class, 'show'])->middleware(['auth'])->name('edit.tabledataapp');
Route::post('/edit-data-app/{id}', [AppController::class, 'update'])->middleware(['auth'])->name('update.tabledataapp');
Route::delete('/destroy-data-app/{id}', [AppController::class, 'destroy'])->middleware(['auth'])->name('destroy.tabledataapp');
Route::get('/tabledataapp/search',[AppController::class, 'search']);

Route::get('/setting', [SettingController::class, 'index'])->middleware(['auth'])->name('setting');
Route::post('/setting', [SettingController::class, 'update'])->middleware(['auth'])->name('update.setting');







//user
// Route::get('/user/search-results', function () {
//     return view('landing.contents.allResult');
// });

Route::get('/user/search-results/{keyword}', [SearchController::class, 'search'])->name('keyword');


require __DIR__.'/auth.php';
