<?php

namespace App\Http\Controllers;


use App\Models\App;
use App\Models\Pemilik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class AppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('tb_app')->select('tb_app.id', 'tb_app.nama_app','tb_pemilik.nama','tb_app.url')->join('tb_pemilik', 'tb_app.id_pemilik', '=', 'tb_pemilik.id')->where('tb_app.status', '>=', '0')->paginate(4);
        return view('dashboard.tabledataapp', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $datamaster = Pemilik::where('status', '>=', '0')->get();
        return view('dashboard.tambahdataapp', compact('datamaster'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        App::create([
            'nama_app' => $request->nama,
            'id_pemilik' => $request->pemilik,
            'url' => $request->url,
        ]);

        Alert::success('Success Message', 'Success Save');
        $data = DB::table('tb_app')->join('tb_pemilik', 'tb_app.id_pemilik', '=', 'tb_pemilik.id')->where('tb_app.status', '>=', '0')->get();
        return redirect()->route('tabledataapp')->with(['data']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = App::where('id', $id)->first();
        $datamaster = Pemilik::where('status', '>=', '0')->get();
        return view('dashboard.edittableapp', compact('data', 'datamaster'));
    } 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        App::where('id', $id)->update([
            'nama_app' => $request->nama,
            'id_pemilik' => $request->pemilik,
            'url' => $request->url,
        ]);

        Alert::success('Success Message', 'Success Update');
        $data = DB::table('tb_app')->join('tb_pemilik', 'tb_app.id_pemilik', '=', 'tb_pemilik.id')->where('tb_app.status', '>=', '0')->get();
        return redirect()->route('tabledataapp')->with(['data']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        App::where('id', $id)->update([
            'status' => '-1',
        ]);

        Alert::success('Success Message', 'Success Delete');
        $data = DB::table('tb_app')->join('tb_pemilik', 'tb_app.id_pemilik', '=', 'tb_pemilik.id')->where('tb_app.status', '>=', '0')->get();
        return redirect()->route('tabledataapp')->with(['data']);
    }

    public function search(Request $request)
    {
        $query = $request->search;
        $data = DB::table('tb_app')->select('tb_app.id', 'tb_app.nama_app','tb_app.url','tb_app.status','tb_pemilik.nama')->join('tb_pemilik', 'tb_app.id_pemilik' , '=', 'tb_pemilik.id')->where('nama_app', 'like', '%'. $query .'%' )->where('tb_app.status', '>=', '0')->paginate(4);
        $count = $data->count();
        return view('dashboard.tabledataapp',['data'=>$data]);
    }
}
